import React  from 'react';
import { connect } from 'react-redux';

import './connection-controls.css';
import { initializeSocket } from "../../actions/websocket";

class ConnectionControls extends React.Component {
  constructor(props) {
    super(props);

    this.toggleConnection = this.toggleConnection.bind(this);
  }

  toggleConnection() {
    this.props.connected
    ? this.props.socket.close()
    : this.props.dispatch(initializeSocket())
  }

  render() {
    const { connected } = this.props;
    const buttonClass = `btn btn-${connected ? 'success' : 'danger'}`;

    return(
      <div className="connection-controls">
        Status: { connected ? 'Active' : 'Off' }

        <button
          className={`float-right btn btn-${buttonClass}`}
          onClick={this.toggleConnection}
        >
          { connected ? 'Stop' : 'Activate' } connection
        </button>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { connected, socket } = state.socket;

  return { connected, socket };
}

export default connect(mapStateToProps)(ConnectionControls);