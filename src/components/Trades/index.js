import React, { Component } from 'react';
import {connect} from "react-redux";

class Trades extends Component {
  render() {
    return (
      <div>
        <h3>Trades</h3>

        <div className="tickers-list">
          <table className="table">
            <thead>
              <tr>
                <th />
                <th>Time</th>
                <th>Price</th>
                <th>Amount</th>
              </tr>
            </thead>

            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { socket } = state.socket;

  return { socket };
}

export default connect(mapStateToProps)(Trades);
