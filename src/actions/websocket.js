import {
  SOCKET_CONNECTION_INIT,
  SOCKET_CONNECTION_SUCCESS,
  SOCKET_CONNECTION_ERROR,
  SOCKET_CONNECTION_CLOSED,
  SOCKET_MESSAGE
} from "./index";


function socketConnectionInit(socket) {
  return {
    type: SOCKET_CONNECTION_INIT,
    socket,
  };
}

function socketConnectionSuccess() {
  return {
    type: SOCKET_CONNECTION_SUCCESS,
  };
}

function socketConnectionError() {
  return {
    type: SOCKET_CONNECTION_ERROR,
  };
}

function socketConnectionClosed() {
  return {
    type: SOCKET_CONNECTION_CLOSED,
  };
}

function socketMessage(data) {
  return {
    type: SOCKET_MESSAGE,
    data,
  };
}

export function initializeSocket() {
  return (dispatch) => {
    const socket = new WebSocket('wss://api.bitfinex.com/ws/2');

    dispatch(socketConnectionInit(socket));

    socket.onopen = function() {
      dispatch(socketConnectionSuccess());
    };

    socket.onerror = function() {
      dispatch(socketConnectionError());
    };

    socket.onmessage = function (event) {
      dispatch(socketMessage(event.data));
    };

    socket.onclose = function() {
      dispatch(socketConnectionClosed());
    };
  };
}