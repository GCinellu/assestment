import { combineReducers } from 'redux';
import socket from './websocket';

const rootReducer = combineReducers({
  socket,
});

export default rootReducer;