import {
  SOCKET_CONNECTION_INIT,
  SOCKET_CONNECTION_SUCCESS,
  SOCKET_CONNECTION_ERROR,
  SOCKET_CONNECTION_CLOSED,
  SOCKET_MESSAGE
} from "./../actions";

const initialState = {
  connected: false,
  readyState: null,
  socket: null,
};

export default function websocketReducer(state = initialState, action = {}) {
  switch (action.type) {
    case SOCKET_CONNECTION_INIT:
      return Object.assign({}, state, {
        connected: false,
        socket: action.socket,
      });

    case SOCKET_CONNECTION_SUCCESS:
      return Object.assign({}, state, {
        connected: true,
      });

    case SOCKET_CONNECTION_ERROR:
      return Object.assign({}, state, {
        connected: false,
      });

    case SOCKET_CONNECTION_CLOSED:
      return Object.assign({}, state, {
        connected: false,
        socket: null,
      });

    case SOCKET_MESSAGE:
      return state;

    default:
      return state;
  }
}