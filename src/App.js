import React  from 'react';
import { connect } from 'react-redux';
import { initializeSocket } from './actions/websocket';

import Trades from './components/Trades'
import Header from "./components/Header";
import ConnectionControls from './components/ConnectionControls'

class App extends React.Component {
  componentWillMount() {
    const { dispatch } = this.props;

    dispatch(initializeSocket());
  }

  render() {
    return(
      <div id="mainWrapper" className="main-grid clearfix flexbox">
        <Header />

        <div className="container">
          <div className="row">
            <div id='sidebar' className="col-4">
              <ConnectionControls />
            </div>

            <div className="col-8">
              <Trades />
            </div>
          </div>
        </div>


      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    socket: state.socket,
  };
}

export default connect(mapStateToProps)(App);